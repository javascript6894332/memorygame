function memoryGame() {
  let cardsArray = [
    { imgsrc: "images/icons8-adventure-94.png", name: "adventure" },
    { imgsrc: "images/icons8-adventure-94.png", name: "adventure" },
    { imgsrc: "images/icons8-archer-94.png", name: "archer" },
    { imgsrc: "images/icons8-archer-94.png", name: "archer" },
    { imgsrc: "images/icons8-bishop-94.png", name: "bishop" },
    { imgsrc: "images/icons8-bishop-94.png", name: "bishop" },
    { imgsrc: "images/icons8-gun-94.png", name: "gun" },
    { imgsrc: "images/icons8-gun-94.png", name: "gun" },
    { imgsrc: "images/icons8-joystick-94.png", name: "joystick" },
    { imgsrc: "images/icons8-joystick-94.png", name: "joystick" },
    { imgsrc: "images/icons8-knight-94.png", name: "knight" },
    { imgsrc: "images/icons8-knight-94.png", name: "knight" },
    { imgsrc: "images/icons8-queen-94.png", name: "queen" },
    { imgsrc: "images/icons8-queen-94.png", name: "queen" },
    { imgsrc: "images/icons8-virtual-reality-94.png", name: "virtual-reality" },
    { imgsrc: "images/icons8-virtual-reality-94.png", name: "virtual-reality" },
  ];
  
  let cardAdd = true;
  let timer = 0;
  let moves = 0;
  let matched = 0;
  let timerInterval;
  let gameStarted = false;
  const startbtn = document.getElementById("head-start-btn");
  const cardsContainer = document.getElementById("cards");
  function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

  shuffleArray(cardsArray);

  function addCard() {
    cardAdd = false;
    cardsArray.forEach((backContent) => {
      const card = document.createElement("div");
      card.className = "card";
      const cardInner = document.createElement("div");
      cardInner.className = "card-inner";
      const cardFront = document.createElement("div");
      cardFront.className = "card-front";

      const cardBack = document.createElement("div");
      cardBack.className = "card-back";
      const cardIcon = document.createElement("img");
      cardIcon.src = backContent.imgsrc;
      card.setAttribute("name", backContent.name);
      cardBack.appendChild(cardIcon);
      cardInner.appendChild(cardFront);
      cardInner.appendChild(cardBack);
      card.appendChild(cardInner);
      card.addEventListener("click", (event) => {
        card.classList.toggle("flipped");
        if (gameStarted){
          checkCards(event);
        }
      });

      cardsContainer.appendChild(card);
    });
  }

  function checkCards(event) {
    const clickedCard = event.target;
    clickedCard.classList.add("flipTrue");
    const flipped = document.querySelectorAll(".flipTrue");
    if (flipped.length === 2) {
      moves += 2;
      if (flipped[0].getAttribute("name") === flipped[1].getAttribute("name")) {
        flipped.forEach((card) => {
          card.classList.remove("flipTrue");
          card.style.pointerEvents = "none";
        });
        matched += 2;
        if (matched === cardsArray.length) {
          clearInterval(timerInterval);
          startbtn.disabled = false;
          startbtn.style.color = "white";
          document.getElementById(
            "message"
          ).textContent = `congrats, you took ${moves} moves and, ${timer} seconds.`;
          exit();
        }
      } else {
        flipped.forEach((card) => {
          card.classList.remove("flipTrue");
          setTimeout(() => {
            card.classList.remove("flipped");
          }, 1000);
        });
      }
      document.getElementById("moves").textContent = moves;
    }
  }

  function startGame() {
    gameStarted = true;
    timerInterval = setInterval(() => {
      timer++;
      document.getElementById("timer").textContent = timer;
    }, 1000);
    startbtn.disabled = true;
    startbtn.style.color = "grey";
  }

  startbtn.addEventListener("click", startGame);
  if (cardAdd){
    addCard();
  }
  
}

memoryGame();
